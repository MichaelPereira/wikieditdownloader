/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.stevens.decisioncenter.wikieditsdownloader.library.output;

import edu.stevens.decisioncenter.wikieditsdownloader.library.*;
import edu.stevens.decisioncenter.wikieditsdownloader.library.output.db.Pages;
import edu.stevens.decisioncenter.wikieditsdownloader.library.output.db.Pagesrevisions;

/**
 *
 * @author michaelpereira
 */
public interface Output {
    void saveEdit(Edit e);
    void saveRevision(Pagesrevisions r);
    void savePage(Pages p);
}
