/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.stevens.decisioncenter.wikieditsdownloader.library.output;

import edu.stevens.decisioncenter.wikieditsdownloader.library.output.db.*;
import java.util.Date;
import java.util.HashSet;
import org.hibernate.Session;

/**
 *
 * @author michaelpereira
 */
public class DatabaseOutputImpl implements Output {

    private Session session;
    
    public DatabaseOutputImpl() {
        session = HibernateUtil.getSessionFactory().getCurrentSession();
    }

    
    
    public void saveEdit(edu.stevens.decisioncenter.wikieditsdownloader.library.Edit e) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void saveRevision(Pagesrevisions r) {
        if (!session.isOpen())
            session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(r);
        session.getTransaction().commit();
    }

    public void savePage(Pages p) {
        if (!session.isOpen())
            session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(p);
        session.getTransaction().commit();
    }
    
}
