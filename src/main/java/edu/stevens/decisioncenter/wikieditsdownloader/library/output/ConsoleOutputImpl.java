/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.stevens.decisioncenter.wikieditsdownloader.library.output;

import edu.stevens.decisioncenter.wikieditsdownloader.library.Edit;
import edu.stevens.decisioncenter.wikieditsdownloader.library.Revision;
import edu.stevens.decisioncenter.wikieditsdownloader.library.output.db.Pages;
import edu.stevens.decisioncenter.wikieditsdownloader.library.output.db.Pagesrevisions;

/**
 *
 * @author michaelpereira
 */
public class ConsoleOutputImpl implements Output {

    public void saveEdit(Edit e) {
        System.out.println(e);
    }

    public void saveRevision(Pagesrevisions r) {
        System.out.println(r.getDate() + " " + r.getRevisionid() + " " + r.getAuthor() + " " + r.getMessage());
    }

    public void savePage(Pages p) {
        System.out.println(p);
        for (Pagesrevisions rev : p.getPagesrevisionses()) {
            System.out.println(String.format("\t %s", rev));
        }
    }
    
}
