/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.stevens.decisioncenter.wikieditsdownloader.library;

import edu.stevens.decisioncenter.wikieditsdownloader.library.output.*;
import edu.stevens.decisioncenter.wikieditsdownloader.library.output.db.Pages;
import edu.stevens.decisioncenter.wikieditsdownloader.library.output.db.Pagesrevisions;
import java.io.*;
import java.net.*;
import java.text.MessageFormat;
import java.util.*;
import java.util.logging.*;
import javax.xml.parsers.*;
import javax.xml.xpath.*;
import org.w3c.dom.*;
import org.xml.sax.*;

/**
 *
 * @author michaelpereira
 */
public class WikiPagesDownloader {

    private String target;
    private int remainingLevels;
    private List<Pages> pages;
    private Stack<Pages> categories;
    private Output output;
    private final String targetQueryBase = "action=query&format=xml&list=categorymembers&cmtitle={0}&cmsort=sortkey&cmdir=asc&cmlimit=max";
    private String targetQuery;

    public WikiPagesDownloader(String target, int remainingLevels) {
        this.target = target;
        this.remainingLevels = remainingLevels;
        output = new ConsoleOutputImpl();
        targetQuery = MessageFormat.format(targetQueryBase, target);
        pages = new ArrayList<Pages>();
        categories = new Stack<Pages>();
    }

    public WikiPagesDownloader(String target, int remainingLevels, Output output) {
        this.target = target;
        this.remainingLevels = remainingLevels;
        this.output = output;
        targetQuery = MessageFormat.format(targetQueryBase, target);
        pages = new ArrayList<Pages>();
        categories = new Stack<Pages>();
    }

    public List<Pages> getPages() {
        return pages;
    }

    public void setPages(List<Pages> pages) {
        this.pages = pages;
    }

    public void processTargetPage() {
        try {
            XPathFactory factory = XPathFactory.newInstance();
            XPath xpath = factory.newXPath();

            Document doc = getXMLDocument("&cmlimit=max", targetQuery);
            processContent(xpath, doc);

            while ((Boolean) xpath.evaluate("boolean(//query-continue/categorymembers)", doc, XPathConstants.BOOLEAN)) {
                XPathExpression queryContinueExpr = xpath.compile("//query-continue/categorymembers/@cmcontinue");
                String continueValue = (String) queryContinueExpr.evaluate(doc, XPathConstants.STRING);
                doc = getXMLDocument(MessageFormat.format("&cmlimit=max&cmcontinue={0}", continueValue), targetQuery);
                processContent(xpath, doc);
            }

        } catch (XPathExpressionException ex) {
            Logger.getLogger(WikiPagesDownloader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(WikiPagesDownloader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(WikiPagesDownloader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(WikiPagesDownloader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MalformedURLException ex) {
            Logger.getLogger(WikiPagesDownloader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WikiPagesDownloader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void processContent(XPath xpath, Document doc) throws XPathExpressionException {
        NodeList articleNodes = retrieveNodes(xpath, "0", doc);
        NodeList categoryNodes = retrieveNodes(xpath, "14", doc);
        retrievePages(xpath, articleNodes, pages);
        retrievePages(xpath, categoryNodes, categories);
    }

    private void retrievePages(XPath xpath, NodeList articleNodes, List<Pages> pageList) throws XPathExpressionException {
        XPathExpression titleExpr = xpath.compile("@title");
        XPathExpression pageidExpr = xpath.compile("@pageid");
        XPathExpression nsExpr = xpath.compile("@ns");
        for (int i = 0; i < articleNodes.getLength(); i++) {
            Node currentNode = articleNodes.item(i);
            final String pageId = (String) pageidExpr.evaluate(currentNode, XPathConstants.STRING);
            final String title = (String) titleExpr.evaluate(currentNode, XPathConstants.STRING);
            final String ns = (String) nsExpr.evaluate(currentNode, XPathConstants.STRING);
            Pages page = new Pages(title, ns, new HashSet<Pagesrevisions>());
            pageList.add(page);
        }
    }

    private NodeList retrieveNodes(XPath xpath, String namespace, Document doc) throws XPathExpressionException {
        XPathExpression expr = xpath.compile(MessageFormat.format("//cm[@ns={0}]", namespace));
        Object result = expr.evaluate(doc, XPathConstants.NODESET);
        NodeList nodes = (NodeList) result;
        return nodes;
    }

    private Document getXMLDocument(String suffix, String customTargetQuery) throws URISyntaxException, MalformedURLException, IOException, ParserConfigurationException, SAXException {
        URI uri = new URI("http", "en.wikipedia.org", "/w/api.php", customTargetQuery.concat(suffix), null);
        URL url = uri.toURL();
        url.openConnection();
        InputStream inputStream = url.openStream();

        DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
        domFactory.setNamespaceAware(true); // never forget this!
        DocumentBuilder builder = domFactory.newDocumentBuilder();
        return builder.parse(inputStream);
    }

    public Stack<Pages> getCategories() {
        return categories;
    }

    public void setCategories(Stack<Pages> categories) {
        this.categories = categories;
    }
}
