package edu.stevens.decisioncenter.wikieditsdownloader;

import edu.stevens.decisioncenter.wikieditsdownloader.library.output.db.Pages;
import edu.stevens.decisioncenter.wikieditsdownloader.library.WikiPagesDownloader;
import edu.stevens.decisioncenter.wikieditsdownloader.library.output.DatabaseOutputImpl;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.TreeMap;

/**
 * Hello world!
 *
 */
public class App {

    static Map<Integer, Stack<Pages>> categoriesLevels;
    static List<Pages> pages;

    public static void main(String[] args) {
        DatabaseOutputImpl databaseOutput = new DatabaseOutputImpl();
        getRevisionsFor("Category:Economy_of_the_United_States", databaseOutput);
        getRevisionsFor("Category:Economy_of_the_European_Union", databaseOutput);
    }

    private static void getRevisionsFor(String category, DatabaseOutputImpl databaseOutput) {
        pages = new LinkedList<Pages>();
        categoriesLevels = new TreeMap<Integer, Stack<Pages>>();
        categoriesLevels.put(0, new Stack<Pages>());
        categoriesLevels.put(1, new Stack<Pages>());
        categoriesLevels.put(2, new Stack<Pages>());
        int level = 0;
        
        getLevel(category, level);
        for (Pages page : pages) {
            System.out.println("saving " + page.getTitle());
            databaseOutput.savePage(page);
            WikiRevisionDownloader revisionsDownloader = new WikiRevisionDownloader(page, databaseOutput);
            revisionsDownloader.getRevisions();
            System.out.println("saved " + page.getTitle());
        }
    }

    private static void getLevel(String categoryName, int level) {
        WikiPagesDownloader pageDownloader = new WikiPagesDownloader(categoryName, level);
        pageDownloader.processTargetPage();
        pages.addAll(pageDownloader.getPages());
        categoriesLevels.get(level).addAll(pageDownloader.getCategories());
        if (level < 2) {
            Stack<Pages> currentCategories = categoriesLevels.get(level);

            while (!currentCategories.empty()) {
                System.out.println("sub cat" + currentCategories.peek().getTitle());
                getLevel(currentCategories.pop().getTitle(), level + 1);
            }

        }
    }
}