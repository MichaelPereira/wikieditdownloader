/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.stevens.decisioncenter.wikieditsdownloader;

import edu.stevens.decisioncenter.wikieditsdownloader.library.*;
import edu.stevens.decisioncenter.wikieditsdownloader.library.output.*;
import edu.stevens.decisioncenter.wikieditsdownloader.library.output.db.Pages;
import edu.stevens.decisioncenter.wikieditsdownloader.library.output.db.Pagesrevisions;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;
import java.util.logging.*;
import javax.xml.parsers.*;
import javax.xml.xpath.*;
import org.w3c.dom.*;
import org.xml.sax.*;

/**
 *
 * @author Michael
 */
class WikiRevisionDownloader {

    private Pages page;
    private Output output;
    private final String pagePropsUrl = "action=query&prop=revisions&format=xml&titles={0}&rvprop=timestamp|user|comment|size|flags|ids&rvlimit=max&rvdir=newer";
    private String targetQuery;

    public WikiRevisionDownloader(Pages page, Output output) {
        this.output = output;
        this.page = page;
        targetQuery = MessageFormat.format(pagePropsUrl, page.getTitle());
    }

    WikiRevisionDownloader(Pages page) {
        this.page = page;
        this.output = new ConsoleOutputImpl();
        targetQuery = MessageFormat.format(pagePropsUrl, page.getTitle());
    }

    public void getRevisions() {
        try {
            XPathFactory factory = XPathFactory.newInstance();
            XPath xpath = factory.newXPath();

            Document doc = getXMLDocument("&rvstart=1262304000", targetQuery);
            processContent(xpath, doc);

            while ((Boolean) xpath.evaluate("boolean(//query-continue/revisions)", doc, XPathConstants.BOOLEAN)) {
                XPathExpression queryContinueExpr = xpath.compile("//query-continue/revisions/@rvstartid");
                String continueValue = (String) queryContinueExpr.evaluate(doc, XPathConstants.STRING);
                doc = getXMLDocument(MessageFormat.format("&rvstartid={0}", continueValue), targetQuery);
                processContent(xpath, doc);
            }

        } catch (ParseException ex) {
            Logger.getLogger(WikiRevisionDownloader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (XPathExpressionException ex) {
            Logger.getLogger(WikiPagesDownloader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(WikiPagesDownloader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(WikiPagesDownloader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(WikiPagesDownloader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MalformedURLException ex) {
            Logger.getLogger(WikiPagesDownloader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WikiPagesDownloader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void processContent(XPath xpath, Document doc) throws XPathExpressionException, ParseException {
        NodeList revisionNodes = retrieveNodes(xpath, doc);
        retrieveRevisions(xpath, revisionNodes);
    }

    private void retrieveRevisions(XPath xpath, NodeList articleNodes) throws XPathExpressionException, ParseException {
        XPathExpression timestampExpr = xpath.compile("@timestamp");
        XPathExpression commentExpr = xpath.compile("@comment");
        XPathExpression revisionIdExpr = xpath.compile("@revid");
        XPathExpression parendIdExpr = xpath.compile("@parentid");
        XPathExpression userExpr = xpath.compile("@user");
        XPathExpression sizeExpr = xpath.compile("@size");
        XPathExpression anonExpr = xpath.compile("boolean(@anon)");
        XPathExpression minorExpr = xpath.compile("boolean(@size)");

        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

        for (int i = 0; i < articleNodes.getLength(); i++) {
            Node currentNode = articleNodes.item(i);
            final String timestamp = (String) timestampExpr.evaluate(currentNode, XPathConstants.STRING);
            final Date revDate = inputFormat.parse(timestamp);
            final String comment = (String) commentExpr.evaluate(currentNode, XPathConstants.STRING);
            final Double revisionId = (Double) revisionIdExpr.evaluate(currentNode, XPathConstants.NUMBER);
            final Double parentId = (Double) parendIdExpr.evaluate(currentNode, XPathConstants.NUMBER);
            final String user = (String) userExpr.evaluate(currentNode, XPathConstants.STRING);
            final Double size = (Double) sizeExpr.evaluate(currentNode, XPathConstants.NUMBER);
            
            final Boolean anon = (Boolean) anonExpr.evaluate(currentNode, XPathConstants.BOOLEAN);
            final Boolean minor = (Boolean) minorExpr.evaluate(currentNode, XPathConstants.BOOLEAN);

            Pagesrevisions rev = new Pagesrevisions();
            rev.setAnon(anon);
            rev.setAuthor(user);
            rev.setDate(revDate);
            rev.setMessage(comment);
            rev.setMinor(minor);
            rev.setParentid(parentId.longValue());
            rev.setRevisionid(revisionId.longValue());
            rev.setSize(size.intValue());
            rev.setPages(page);
            page.getPagesrevisionses().add(rev);
            output.saveRevision(rev);
        }
    }

    private NodeList retrieveNodes(XPath xpath, Document doc) throws XPathExpressionException {
        XPathExpression expr = xpath.compile("//rev");
        Object result = expr.evaluate(doc, XPathConstants.NODESET);
        NodeList nodes = (NodeList) result;
        return nodes;
    }

    private Document getXMLDocument(String suffix, String customTargetQuery) throws URISyntaxException, MalformedURLException, IOException, ParserConfigurationException, SAXException {
        URI uri = new URI("http", "en.wikipedia.org", "/w/api.php", customTargetQuery.concat(suffix), null);
        URL url = uri.toURL();
        url.openConnection();
        InputStream inputStream = url.openStream();

        DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
        domFactory.setNamespaceAware(true); // never forget this!
        DocumentBuilder builder = domFactory.newDocumentBuilder();
        return builder.parse(inputStream);
    }
}
